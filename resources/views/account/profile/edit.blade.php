@extends('layouts.wrapper', [
    'pageTitle' => 'Edit Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile.edit') !!}

    <h1>Edit Profile</h1>

    <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
        <div class="form-group row {{ empty($errors->get('first_name')) ? "" : " has-error" }}">
            <label for="first_name" class="col-sm-2 col-form-label">First Name:*</label>
            <div class="col">
                <input type="text" class="form-control" id="first_name" name="first_name"
                       placeholder="First Name" value="{{ old('first_name', $userToEdit->first_name) }}">
            </div>
        </div>
        <div class="form-group row {{ empty($errors->get('last_name')) ? "" : " has-error" }}">
            <label for="last_name" class="col-sm-2 col-form-label">Last Name:*</label>
            <div class="col">
                <input type="text" class="form-control" id="last_name" name="last_name"
                       placeholder="Last Name" value="{{ old('last_name', $userToEdit->last_name) }}">
            </div>
        </div>

        @if($userToEdit->provider == 'local')
            <div class="form-group row {{ empty($errors->get('email')) ? "" : " has-error" }}">
                <label for="email" class="col-sm-2 col-form-label">Email address:*</label>
                <div class="col">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Name"
                           value="{{ old('email', $userToEdit->email) }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-2">
                    <p class="form-text">Reset your password?</p>
                </div>
                <div class="col">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="reset_user_password"
                               @if(old('reset_user_password') !== null)
                               checked
                                @endif
                        >
                        <label class="form-check-label" for="reset_user_password">
                            Reset password
                        </label>
                        <small class="form-text text-muted">Checking this box will invalidate your current password and you a password
                            reset email.
                        </small>
                    </div>
                </div>
            </div>

            <div class="form-group row {{ empty($errors->get('avatar_url')) ? "" : " has-error" }}">
                <label for="avatar_url" class="col-sm-2 col-form-label">Avatar URL:</label>
                <div class="col">
                    <input type="file" class="form-control" id="avatar" name="avatar">
                    @if(!is_null($userToEdit->avatar_url))
                        <small class="form-text text-muted">Current file: <a href="{{ url($userToEdit->avatar_url) }}" target="_blank">{{ $userToEdit->avatar_filename }}</a></small>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col offset-2">
                    <div class="form-check">
                        <input type="checkbox" value="1" name="remove_avatar"
                               @if(old('remove_avatar') !== null)
                               checked
                                @endif
                        >
                        <label for="remove_avatar" class="form-check-label">Remove avatar?</label>
                        <small class="form-text text-muted">Checking this box will remove the user avatar and revert it to the placeholder image.</small>
                    </div>
                </div>
            </div>

        @else
            <p><em>Note: your email address, password, and avatar cannot be changed because you registered using a third party provider: {{ ucwords($userToEdit->provider) }}. Please adjust these items by visiting your account at that provider's website.</em></p>
        @endif

        <div class="form-group row">
            <label for="time_zone" class="col-sm-2 col-form-label">Time Zone:</label>
            <div class="col">
                <select class="form-control" id="time_zone" name="time_zone">
                    @foreach($timezone_list as $value => $display)
                        <option value="{{ $value }}"
                                @if (old('time_zone', $userToEdit->time_zone) == $value)
                                selected
                                @endif
                        >{{ $display }}</option>
                    @endforeach
                </select></div>
        </div>

        {{ csrf_field() }}
        {{ method_field("PATCH") }}

        <div class="form-group row">
            <div class="col-sm-10 offset-2">
                <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-check"></i> Submit</button>
                <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm"><i class="fas fa-times"></i> Cancel</a>
            </div>
        </div>
    </form>

@endsection()