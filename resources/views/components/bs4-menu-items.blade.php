@foreach($items as $item)
    <li @lm-attrs($item) class="m-{{ $margin or '2' }} @if($item->hasChildren()) dropdown @endif " @lm-endattrs>
        @if($item->link) <a @lm-attrs($item->link) @if($item->hasChildren()) class="{{ $linkClass or 'nav-link' }} dropdown-toggle" data-toggle="dropdown" @else class="{{ $linkClass or 'nav-link' }}" @endif @lm-endattrs href="{!! $item->url() !!}">
        {!! $item->title !!}
        @if($item->hasChildren()) <b class="caret"></b> @endif
        </a>
        @else
            {!! $item->title !!}
        @endif
        @if($item->hasChildren())
            <ul class="dropdown-menu">
                @include('components.bs4-menu-items', ['items' => $item->children(), 'linkClass' => 'dropdown-item', 'margin' => '0'])
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach
